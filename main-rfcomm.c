/*
 * This file is part of the libble project.
 *
 * Copyright (C) 2018-2019 Gerhard Sittig <gerhard.sittig@gmx.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Simple RFCOMM server implementation, modelled after
 * http://people.csail.mit.edu/albert/bluez-intro/x502.html
 *
 * Creates a server socket, accepts up to one client at a time, reflects
 * received data to the server (echo mode), terminates upon reception of
 * CTRL-C / SIGTERM and friends.
 *
 * Was specifically written as a standalone application not using libble
 * to demonstrate interoperability.
 */

/* {{{ includes, defines, globals */

#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>
#include <poll.h>
#include <signal.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>

#ifndef ARRAY_SIZE
#  define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))
#endif

static bdaddr_t local_addr = *BDADDR_ANY;
static int channel = 1;

static int log_level = 1;
static int done = 0;

/* }}} includes, defines, globals */
/* {{{ logging, progress messages */

static void log_msg(int level, const char *fmt, ...)
{
	va_list arg;
	FILE *log_stream;

	if (level > log_level)
		return;

	log_stream = stdout;
	va_start(arg, fmt);
	vfprintf(log_stream, fmt, arg);
	va_end(arg);
	fflush(log_stream);
}

/* }}} logging, progress messages */
/* {{{ helper routines */

static int server_socket(void)
{
	int fd;
	struct sockaddr_rc addr;
	int rc;

	log_msg(2, "socket()\n");
	fd = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
	if (fd < 0) {
		perror("socket()");
		return -1;
	}

	log_msg(2, "bind()\n");
	memset(&addr, 0, sizeof(addr));
	addr.rc_family = AF_BLUETOOTH;
	addr.rc_channel = channel;
	memcpy(&addr.rc_bdaddr, &local_addr, sizeof(addr.rc_bdaddr));
	rc = bind(fd, (struct sockaddr *)&addr, sizeof(addr));
	if (rc < 0) {
		perror("bind()");
		close(fd);
		return -1;
	}

	log_msg(2, "listen()\n");
	rc = listen(fd, -1);
	if (rc < 0) {
		perror("listen()");
		close(fd);
		return -1;
	}

	log_msg(1, "listening ...\n");
	return fd;
}

static void close_server(int fd)
{

	log_msg(2, "close() server\n");
	close(fd);
	log_msg(1, "server closed\n");
}

static int accept_client(int srv_fd)
{
	int cli_fd;
	socklen_t len;
	struct sockaddr_rc addr;
	char peer[20];

	if (srv_fd < 0)
		return -1;

	log_msg(2, "accept()\n");
	len = sizeof(addr);
	cli_fd = accept(srv_fd, (struct sockaddr *)&addr, &len);
	if (cli_fd < 0) {
		perror("accept()");
		return -1;
	}
	ba2str(&addr.rc_bdaddr, peer);
	log_msg(1, "accept: fd %d, client addr %s\n", cli_fd, peer);
	return cli_fd;
}

static int process_client(int fd, const char *msg)
{
	ssize_t rdlen, wrlen;

	rdlen = strlen(msg);
	log_msg(2, "write()\n");
	wrlen = write(fd, msg, rdlen);
	if (wrlen < 0) {
		perror("write()");
		return -1;
	}
	log_msg(1, "sent: fd %d, data: %s\n", fd, msg);
	if (wrlen != rdlen) {
		log_msg(0, "write mismatch: fd %d, rcvd %zd, sent %zd\n", fd, rdlen, wrlen);
	}
	return 0;
}

static int receive_client(int fd)
{
	char buff[256];
	ssize_t rdlen;
	int rc;

	if (fd < 0)
		return -1;

	log_msg(2, "read()\n");
	rdlen = read(fd, buff, sizeof(buff));
	if (rdlen < 0) {
		perror("read()");
		return -1;
	}
	buff[rdlen] = '\0';
	log_msg(1, "rcvd: fd %d, data: %s\n", fd, buff);
	rc = process_client(fd, buff);
	if (rc < 0)
		return -1;
	return rdlen;
}

static void close_client(int fd)
{

	log_msg(2, "close() client\n");
	close(fd);
	log_msg(1, "close: fd %d, client gone\n", fd);
}

/* }}} helper routines */
/* {{{ main program */

static void signal_handler(int sig)
{

	done++;
}

static int is_readable(int fd)
{
	struct pollfd fds[1];
	int rc;

	memset(fds, 0, sizeof(fds));
	fds[0].fd = fd;
	fds[0].events = POLLIN;
	rc = poll(fds, ARRAY_SIZE(fds), 0);
	if (rc < 1)
		return 0;
	/*
	 * Implementor's note: Silently accept "error" conditions here
	 * as well, to have the socket closed on failed read. Or else
	 * we'd need another has_error() test routine, too.
	 */
	return 1;
}

int main(int argc, char *argv[])
{
	int server_fd, client_fd;
	int rc;

	signal(SIGHUP, signal_handler);
	signal(SIGINT, signal_handler);
	signal(SIGTERM, signal_handler);

	server_fd = server_socket();
	client_fd = -1;
	while (!done) {
		if (is_readable(server_fd)) {
			client_fd = accept_client(server_fd);
			if (client_fd < 0)
				done++;
		}
		if (client_fd >= 0 && is_readable(client_fd)) {
			rc = receive_client(client_fd);
			if (rc <= 0) {
				close_client(client_fd);
				client_fd = -1;
			}
		}
		usleep(10000);
	}
	close_server(server_fd);
	return 0;
}

/* }}} main program */
/*
 * vim:foldmethod=marker:
 */
