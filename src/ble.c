/*
 * This file is part of the libble project.
 *
 * Copyright (C) 2018-2019 Gerhard Sittig <gerhard.sittig@gmx.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* {{{ includes, defined */

#include "ble-config.h"
#include "ble.h"

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/l2cap.h>
#include <bluetooth/rfcomm.h>
#include <ctype.h>
#include <errno.h>
#include <poll.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>

#define STORE_MAC_REVERSE	1
#define ACCEPT_NONSEP_MAC	1

#ifndef ARRAY_SIZE
#  define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))
#endif

/* }}} includes, defined */
/* {{{ version information */

int ble_get_version_info(const char **lib_name, const char **lib_vers)
{

	if (lib_name)
		*lib_name = LIBBLE_LIBNAME;
	if (lib_vers)
		*lib_vers = LIBBLE_LIBVERS;
	return 0;
}

int ble_get_system_info(const char **sys_name, const char **sys_vers)
{

	if (sys_name)
		*sys_name = LIBBLE_SYSNAME;
	if (sys_vers)
		*sys_vers = LIBBLE_SYSVERS;
	return 0;
}

/* }}} version information */
/* {{{ compat decls */
/*
 * The availability of conversion helpers in <bluetooth/bluetooth.h>
 * appears to be version dependent. Let's provide the helper here if
 * the header doesn't.
 */

#if !HAVE_BT_PUT_LE16
static inline void bt_put_le16(uint16_t v, uint8_t *p)
{

	p[0] = (v >> 0) & 0xff;
	p[1] = (v >> 8) & 0xff;
}
#endif

/* }}} compat decls */
/* {{{ Linux socket specific decls */

#define BLE_ATT_ERROR_RESP		0x01
#define BLE_ATT_EXCHANGE_MTU_REQ	0x02
#define BLE_ATT_EXCHANGE_MTU_RESP	0x03
#define BLE_ATT_FIND_INFORMATION_REQ	0x04
#define BLE_ATT_FIND_INFORMATION_RESP	0x05
#define BLE_ATT_FIND_BY_TYPE_REQ	0x06
#define BLE_ATT_FIND_BY_TYPE_RESP	0x07
#define BLE_ATT_READ_BY_TYPE_REQ	0x08
#define BLE_ATT_READ_BY_TYPE_RESP	0x09
#define BLE_ATT_READ_REQ		0x0a
#define BLE_ATT_READ_RESP		0x0b
#define BLE_ATT_READ_BLOB_REQ		0x0c
#define BLE_ATT_READ_BLOB_RESP		0x0d
#define BLE_ATT_READ_MULTIPLE_REQ	0x0e
#define BLE_ATT_READ_MULTIPLE_RESP	0x0f
#define BLE_ATT_READ_BY_GROUP_REQ	0x10
#define BLE_ATT_READ_BY_GROUP_RESP	0x11
#define BLE_ATT_WRITE_REQ		0x12
#define BLE_ATT_WRITE_RESP		0x13
#define BLE_ATT_WRITE_CMD		0x16
#define BLE_ATT_HANDLE_NOTIFICATION	0x1b
#define BLE_ATT_HANDLE_INDICATION	0x1d
#define BLE_ATT_HANDLE_CONFIRMATION	0x1e
#define BLE_ATT_SIGNED_WRITE_CMD	0x52

/* }}} Linux socket specific decls */
/* {{{ conversion */

/*
 * Convert textual MAC presentation to array of bytes. In contrast to
 * BlueZ conversion, accept colon or dash separated input as well as a
 * dense format without separators (001122334455). We expect to use the
 * library in an environment where colons are not always available as a
 * separator in user provided specs, while users do want to use some
 * separator for readability.
 *
 * TODO Instead of doing the actual conversion here (and dealing with
 * BlueZ' internal byte order for device address bytes), we might as
 * well just transform the input string to an output string, and always
 * use the officially provided str2ba() conversion routine.
 */
int ble_mac_text_to_bytes(const char *text, uint8_t *buff)
{
	size_t len;
	long v;
	char *endp;
	char numbuff[3];

	len = 6;
	if (STORE_MAC_REVERSE)
		buff += len;
	endp = (char *)text;
	while (len && endp && *endp) {
		text = endp;
		if (ACCEPT_NONSEP_MAC) {
			numbuff[0] = endp[0];
			numbuff[1] = endp[0] ? endp[1] : '\0';
			numbuff[2] = '\0';
		}
		endp = NULL;
		v = strtol(ACCEPT_NONSEP_MAC ? numbuff : text, &endp, 16);
		if (!endp)
			break;
		if (*endp != ':' && *endp != '-' && *endp != '\0')
			break;
		if (v < 0 || v > 255)
			break;
		if (STORE_MAC_REVERSE)
			*(--buff) = v;
		else
			*buff++ = v;
		len--;
		if (ACCEPT_NONSEP_MAC)
			endp = (char *)text + (endp - numbuff);
		if (*endp == ':' || *endp == '-')
			endp++;
	}

	if (len) {
		ble_log_append(1, "Failed to parse MAC, too few bytes in '%s'\n", text);
		return -1;
	}
	while (isspace(*endp))
		endp++;
	if (*endp) {
		ble_log_append(1, "Failed to parse MAC, excess data in '%s'\n", text);
		return -1;
	}

	return 0;
}

/* }}} conversion */
/* {{{ helpers */

const char *ble_adapter_get_address(size_t idx)
{
	int rc;
	struct hci_dev_info info;
	char addr[20];

	rc = hci_devinfo(idx, &info);
	ble_log_append(0, "DIAG: hci_devinfo(%zu) => rc %d\n", idx, rc);
	if (rc < 0)
		return NULL;
	rc = ba2str(&info.bdaddr, addr);
	ble_log_append(0, "DIAG: ba2str() => rc %d\n", rc);
	if (rc < 0)
		return NULL;
	return strdup(addr);
}

/* }}} helpers */
/* {{{ descriptor */

struct ble_desc {
	/* User servicable options. */
	ble_scan_cb scan_cb;
	void *scan_cb_data;
	ble_data_cb data_cb;
	void *data_cb_data;
	char local_addr[20];
	char remote_addr[20];
	size_t rfcomm_channel;
	uint16_t read_handle;
	uint16_t write_handle;
	uint16_t init_handle;
	uint16_t init_value;
	/* Internal state. */
	int devid;
	int fd;
	struct hci_filter orig_filter;
};

static int ble_desc_open(struct ble_desc *desc, int *id_ref);
static void ble_desc_close(struct ble_desc *desc);
static int ble_check_socket_usable(struct ble_desc *desc);
static ssize_t ble_write_type(struct ble_desc *desc, uint8_t type);
static ssize_t ble_write_type_handle(struct ble_desc *desc, uint8_t type, uint16_t handle);
static ssize_t ble_write_type_handle_bytes(struct ble_desc *desc, uint8_t type, uint16_t handle, const uint8_t *data, size_t len);
static ssize_t ble_char_write_req(struct ble_desc *desc, uint16_t handle, const void *data, size_t len);

struct ble_desc *ble_desc_new(void)
{
	struct ble_desc *desc;

	ble_log_append(3, "BLE desc alloc\n");
	desc = calloc(1, sizeof(*desc));
	if (!desc)
		return NULL;

	desc->devid = -1;
	desc->fd = -1;

	return desc;
}

void ble_desc_free(struct ble_desc *desc)
{

	if (!desc)
		return;

	ble_log_append(3, "BLE desc free\n");
	ble_desc_close(desc);
	free(desc);
}

int ble_config_cb_scan(struct ble_desc *desc, ble_scan_cb cb, void *cb_data)
{

	if (!desc)
		return -1;

	desc->scan_cb = cb;
	desc->scan_cb_data = cb_data;
	return 0;
}

int ble_config_cb_data(struct ble_desc *desc, ble_data_cb cb, void *cb_data)
{

	if (!desc)
		return -1;

	desc->data_cb = cb;
	desc->data_cb_data = cb_data;
	return 0;
}

int ble_config_addr_local(struct ble_desc *desc, const char *addr)
{
	bdaddr_t mac_bytes;
	int rc;

	if (!desc)
		return -1;

	if (!addr || !addr[0]) {
		desc->local_addr[0] = '\0';
		return 0;
	}

	ble_log_append(3, "local addr %s\n", addr);
	rc = ble_mac_text_to_bytes(addr, &mac_bytes.b[0]);
	if (rc < 0)
		return -1;

	rc = ba2str(&mac_bytes, desc->local_addr);
	if (rc < 0)
		return -1;

	return 0;
}

int ble_config_addr_remote(struct ble_desc *desc, const char *addr)
{
	bdaddr_t mac_bytes;
	int rc;

	if (!desc)
		return -1;

	if (!addr || !addr[0]) {
		desc->remote_addr[0] = '\0';
		return 0;
	}

	ble_log_append(3, "local addr %s\n", addr);
	rc = ble_mac_text_to_bytes(addr, &mac_bytes.b[0]);
	if (rc < 0)
		return -1;

	rc = ba2str(&mac_bytes, desc->remote_addr);
	if (rc < 0)
		return -1;

	return 0;
}

int ble_config_rfcomm(struct ble_desc *desc, size_t channel)
{

	if (!desc)
		return -1;

	desc->rfcomm_channel = channel;
	return 0;
}

int ble_config_notify(struct ble_desc *desc,
	uint16_t read_handle, uint16_t write_handle,
	uint16_t init_handle, uint16_t init_value)
{

	if (!desc)
		return -1;

	desc->read_handle = read_handle;
	desc->write_handle = write_handle;
	desc->init_handle = init_handle;
	desc->init_value = init_value;
	return 0;
}

static int ble_desc_open(struct ble_desc *desc, int *id_ref)
{
	int id, sock;
	bdaddr_t mac;

	if (!desc)
		return -1;
	ble_log_append(3, "BLE open\n");

	ble_log_append(4, "get devid\n");
	if (desc->local_addr[0]) {
		id = hci_devid(desc->local_addr);
	} else if (desc->remote_addr[0]) {
		str2ba(desc->remote_addr, &mac);
		id = hci_get_route(&mac);
	} else {
		id = hci_get_route(NULL);
	}
	if (id < 0) {
		ble_log_append(4, "devid failed\n");
		return -1;
	}
	desc->devid = id;
	if (id_ref)
		*id_ref = id;
	ble_log_append(4, "open HCI socket\n");
	sock = hci_open_dev(id);
	if (sock < 0) {
		perror("open HCI socket");
		return -1;
	}
	desc->fd = sock;
	return sock;
}

static void ble_desc_close(struct ble_desc *desc)
{

	if (!desc)
		return;

	ble_log_append(3, "BLE close\n");
	if (desc->fd >= 0) {
		hci_close_dev(desc->fd);
		desc->fd = -1;
	}
	desc->devid = -1;
}

/* }}} descriptor */
/* {{{ scan */

/*
 * Scan support for BLE devices is based on the MIT licensed
 * https://github.com/carsonmcdonald/bluez-experiments scantest.c
 * example source code.
 */

#define EIR_NAME_COMPLETE	9

static int ble_scan_prep(struct ble_desc *desc)
{
	int rc;
	uint8_t type, owntype, filter;
	uint16_t ival, window;
	int timeout;
	uint8_t enable, dup;
	socklen_t slen;
	struct hci_filter scan_filter;

	if (!desc)
		return -1;
	ble_log_append(3, "BLE scan prep\n");

	/* TODO Replace magic values with symbolic identifiers. */
	ble_log_append(4, "set LE scan params\n");
	type = 0x01;	/* LE public? */
	ival = htobs(0x0010);
	window = htobs(0x0010);
	owntype = 0x00;	/* any? */
	filter = 0x00;
	timeout = 1000;
	rc = hci_le_set_scan_parameters(desc->fd, type, ival, window, owntype, filter, timeout);
	if (rc < 0) {
		perror("set LE scan params");
		return -1;
	}

	ble_log_append(4, "set LE scan enable\n");
	enable = 1;
	dup = 1;
	timeout = 1000;
	rc = hci_le_set_scan_enable(desc->fd, enable, dup, timeout);
	if (rc < 0) {
		perror("set LE scan enable");
		return -1;
	}

	/* Save the current filter. For later restoration. */
	ble_log_append(4, "get HCI filter\n");
	slen = sizeof(desc->orig_filter);
	rc = getsockopt(desc->fd, SOL_HCI, HCI_FILTER, &desc->orig_filter, &slen);
	if (rc < 0) {
		perror("getsockopt(HCI_FILTER)");
		return -1;
	}

	ble_log_append(4, "set HCI filter\n");
	hci_filter_clear(&scan_filter);
	hci_filter_set_ptype(HCI_EVENT_PKT, &scan_filter);
	hci_filter_set_event(EVT_LE_META_EVENT, &scan_filter);
	rc = setsockopt(desc->fd, SOL_HCI, HCI_FILTER, &scan_filter, sizeof(scan_filter));
	if (rc < 0) {
		perror("setsockopt(HCI_FILTER)");
		return -1;
	}

	return 0;
}

static int ble_scan_post(struct ble_desc *desc)
{
	int rc;
	uint8_t enable, dup;
	int timeout;

	if (!desc)
		return -1;
	ble_log_append(3, "BLE scan post\n");

	/* Restore previous HCI filter. */
	ble_log_append(4, "set HCI filter\n");
	rc = setsockopt(desc->fd, SOL_HCI, HCI_FILTER, &desc->orig_filter, sizeof(desc->orig_filter));
	if (rc < 0) {
		perror("setsockopt(HCI_FILTER)");
		return -1;
	}

	ble_log_append(4, "set LE scan enable\n");
	enable = 0;
	dup = 1;
	timeout = 1000;
	rc = hci_le_set_scan_enable(desc->fd, enable, dup, timeout);
	if (rc < 0)
		return -1;

	return 0;
}

static int ble_scan_proc(struct ble_desc *desc, ble_scan_cb scan_cb, void *cb_data,
	uint8_t *data, size_t dlen, le_advertising_info *info)
{
	uint8_t type;
	char addr[20];
	const char *name;

	type = data[0];
	if (type == EIR_NAME_COMPLETE) {
		ba2str(&info->bdaddr, addr);
		name = strndup(&data[1], dlen - 1);
		if (scan_cb)
			scan_cb(cb_data, addr, name);
		free((void *)name);
		return 0;
	}

	/* Unknown or unsupported type, ignore silently. */
	return 0;
}

int ble_scan_le(struct ble_desc *desc, int duration)
{
	int rc;
	time_t deadline;
	uint8_t buff[HCI_MAX_EVENT_SIZE];
	ssize_t rdlen, rdpos;
	evt_le_meta_event *meta;
	le_advertising_info *info;
	uint8_t *dataptr;
	size_t datalen;

	if (!desc)
		return -1;
	ble_log_append(3, "BLE scan (LE)\n");

	ble_log_append(4, "desc open\n");
	rc = ble_desc_open(desc, NULL);
	if (rc < 0)
		return -1;

	ble_log_append(4, "scan prep\n");
	rc = ble_scan_prep(desc);
	if (rc < 0)
		return -1;

	ble_log_append(4, "scan loop\n");
	deadline = time(NULL);
	deadline += duration;
	while (time(NULL) <= deadline) {

		if (ble_check_socket_usable(desc) < 0)
			break;
		rdlen = ble_read(desc, buff, sizeof(buff));
		if (rdlen < 0)
			break;
		if (!rdlen) {
			ble_log_append(5, "usleep() start\n");
			usleep(50000);
			ble_log_append(5, "usleep() done\n");
			continue;
		}
		if (rdlen < 1 + HCI_EVENT_HDR_SIZE)
			continue;
		meta = (void *)&buff[1 + HCI_EVENT_HDR_SIZE];
		rdlen -= 1 + HCI_EVENT_HDR_SIZE;
		if (meta->subevent != EVT_LE_ADVERTISING_REPORT)
			continue;
		info = (void *)&meta->data[1];
		ble_log_append(4, "evt: type %d, len %d\n", info->evt_type, info->length);
		if (!info->length)
			continue;

		rdpos = 0;
		while (rdpos < rdlen) {
			datalen = info->data[rdpos];
			dataptr = &info->data[1 + rdpos];
			if (rdpos + 1 + datalen > info->length)
				break;
			rdpos += 1 + datalen;
			rc = ble_scan_proc(desc, desc->scan_cb, desc->scan_cb_data, dataptr, datalen, info);
			if (rc < 0)
				break;
		}
	}

	ble_log_append(4, "scan post\n");
	rc = ble_scan_post(desc);
	if (rc < 0)
		return -1;

	ble_desc_close(desc);
	return 0;
}

int ble_scan_bt(struct ble_desc *desc, int duration)
{
	int dev_id, sock, rsp_max;
	long flags;
	inquiry_info *info;
	size_t rsp_count, idx;
	char addr[20];
	char name[256];

	if (!desc)
		return -1;
	ble_log_append(3, "BLE scan (BT)\n");

	sock = ble_desc_open(desc, &dev_id);
	if (sock < 0)
		return -1;

	rsp_max = 255;
	info = calloc(rsp_max, sizeof(*info));
	flags = 0 /* | IREQ_CACHE_FLUSH */;
	rsp_count = hci_inquiry(dev_id, duration, rsp_max, NULL, &info, flags);
	if (rsp_count < 0)
		perror("hci_inquiry");

	for (idx = 0; idx < rsp_count; idx++) {
		memset(addr, 0, sizeof(addr));
		ba2str(&info[idx].bdaddr, addr);
		memset(name, 0, sizeof(name));
		if (hci_read_remote_name(sock, &info[idx].bdaddr, sizeof(name), name, 0) < 0)
			snprintf(name, sizeof(name), "[unknown]");
		if (desc->scan_cb)
			desc->scan_cb(desc->scan_cb_data, addr, name);
	}
	free(info);

	ble_desc_close(desc);
	return 0;
}

/* }}} scan */
/* {{{ connect/disconnect */

int ble_connect_ble(struct ble_desc *desc)
{
	struct sockaddr_l2 sl2;
	bdaddr_t mac;
	int s, ret;

	if (!desc)
		return -1;
	if (!desc->remote_addr[0])
		return -1;
	ble_log_append(3, "BLE connect, remote addr %s\n", desc->remote_addr);

	ble_log_append(4, "socket()\n");
	s = socket(AF_BLUETOOTH, SOCK_SEQPACKET, 0);
	if (s < 0) {
		perror("socket create");
		return s;
	}
	desc->fd = s;

	ble_log_append(4, "bind()\n");
	memset(&sl2, 0, sizeof(sl2));
	sl2.l2_family = AF_BLUETOOTH;
	sl2.l2_psm = 0;
	if (desc->local_addr[0])
		str2ba(desc->local_addr, &mac);
	else
		mac = *BDADDR_ANY;
	memcpy(&sl2.l2_bdaddr, &mac, sizeof(sl2.l2_bdaddr));
	sl2.l2_cid = L2CAP_FC_CONNLESS;
	sl2.l2_bdaddr_type = BDADDR_LE_PUBLIC;
	ret = bind(s, (void *)&sl2, sizeof(sl2));
	if (ret < 0) {
		perror("bind");
		return ret;
	}

	if (0) {
		struct bt_security buff = {
			.level = BT_SECURITY_LOW,
			.key_size = 0,
		};
		ble_log_append(4, "security\n");
		ret = setsockopt(s, SOL_BLUETOOTH, BT_SECURITY, &buff, sizeof(buff));
		if (ret < 0) {
			perror("setsockopt");
			return ret;
		}
	}

	ble_log_append(4, "connect()\n");
	str2ba(desc->remote_addr, &mac);
	memcpy(&sl2.l2_bdaddr, &mac, sizeof(sl2.l2_bdaddr));
	sl2.l2_bdaddr_type = BDADDR_LE_PUBLIC;
	ret = connect(s, (void *)&sl2, sizeof(sl2));
	/*
	 * Cope with "in progress" condition. Keep polling the status
	 * until connect() completes, then get the error by means of
	 * getsockopt(). See the connect(2) manpage for details.
	 */
	if (ret < 0 && errno == EINPROGRESS) {
		struct pollfd fds[1];
		uint32_t soerror;
		socklen_t solen;

		/* TODO
		 * We seem to get here ("connect in progress") even when
		 * the specified peer is not around at all. Which results
		 * in extended periods of time where nothing happens, and
		 * an application timeout seems to be required.
		 */
		ble_log_append(4, "in progress ...\n");

		do {
			ble_log_append(4, "poll(OUT)\n");
			memset(fds, 0, sizeof(fds));
			fds[0].fd = s;
			fds[0].events = POLLOUT;
			ret = poll(fds, ARRAY_SIZE(fds), -1);
			if (ret < 0) {
				perror("poll(OUT)");
				return ret;
			}
			if (!ret)
				continue;
			if (!(fds[0].revents & POLLOUT))
				continue;
		} while (1);
		ble_log_append(4, "poll(INVAL)\n");
		memset(fds, 0, sizeof(fds));
		fds[0].fd = s;
		fds[0].events = POLLNVAL;
		ret = poll(fds, 1, 0);
		if (ret < 0) {
			perror("poll(INVAL)");
			return ret;
		}
		if (ret) {
			/* socket fd is invalid(?) */
			desc->fd = -1;
			close(s);
			return -1;
		}
		ble_log_append(4, "getsocktop(SO_ERROR)\n");
		solen = sizeof(soerror);
		ret = getsockopt(s, SOL_SOCKET, SO_ERROR, &soerror, &solen);
		if (ret < 0) {
			perror("getsockopt(SO_ERROR)");
			return ret;
		}
		if (soerror) {
			/* connect(2) failed, SO_ERROR has the error code. */
			errno = soerror;
			perror("connect(PROGRESS)");
			return soerror;
		}

		/*
		 * TODO Get the receive MTU here?
		 * getsockopt(SOL_BLUETOOTH, BT_RCVMTU, u16);
		 */
	}
	ble_log_append(4, "connect() => rc %d, fd %d\n", ret, desc->fd);
	if (ret < 0) {
		perror("connect");
		return ret;
	}

	return 0;
}

int ble_connect_rfcomm(struct ble_desc *desc)
{
	struct sockaddr_rc addr;
	int fd, rc;

	if (!desc)
		return -1;
	if (!desc->remote_addr[0])
		return -1;
	ble_log_append(3, "RFCOMM connect, remote addr %s, channel %zu\n", desc->remote_addr, desc->rfcomm_channel);

	if (!desc->rfcomm_channel)
		desc->rfcomm_channel = 1;

	ble_log_append(4, "socket()\n");
	fd = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
	if (fd < 0) {
		perror("socket");
		return -1;
	}
	desc->fd = fd;

	ble_log_append(4, "connect()\n");
	memset(&addr, 0, sizeof(addr));
	addr.rc_family = AF_BLUETOOTH;
	str2ba(desc->remote_addr, &addr.rc_bdaddr);
	addr.rc_channel = desc->rfcomm_channel;
	rc = connect(fd, (struct sockaddr *)&addr, sizeof(addr));
	if (rc < 0) {
		perror("connect");
		return -2;
	}

	ble_log_append(4, "connected\n");
	return 0;
}

void ble_disconnect(struct ble_desc *desc)
{

	ble_log_append(3, "BLE disconnect\n");

	if (!desc)
		return;
	ble_desc_close(desc);
}

static int ble_check_socket_usable(struct ble_desc *desc)
{
	struct pollfd fds[1];
	int ret;

	ble_log_append(4, "socket usability check\n");
	if (!desc)
		return -1;
	if (desc->fd < 0)
		return -1;

	memset(fds, 0, sizeof(fds));
	fds[0].fd = desc->fd;
	fds[0].events = POLLERR | POLLHUP;
	ret = poll(fds, ARRAY_SIZE(fds), 0);
	if (ret < 0)
		return ret;
	if (!ret)
		return 0;
	if (fds[0].revents & POLLHUP)
		return -1;
	if (fds[0].revents & POLLERR)
		return -2;
	if (fds[0].revents & POLLNVAL)
		return -3;

	return 0;
}

/* }}} connect/disconnect */
/* {{{ indication/notification */

int ble_start_notify(struct ble_desc *desc)
{
	uint8_t buf[sizeof(desc->init_value)];
	ssize_t wrlen;

	if (!desc)
		return -1;
	ble_log_append(3, "BLE start notify\n");

	if (ble_check_socket_usable(desc) < 0)
		return -2;

	ble_log_append(4, "write()\n");
	bt_put_le16(desc->init_value, buf);
	wrlen = ble_char_write_req(desc, desc->init_handle, buf, sizeof(buf));
	if (wrlen != sizeof(buf))
		return -2;

	return 0;
}

int ble_check_notify(struct ble_desc *desc)
{
	uint8_t buf[1024];
	ssize_t rdlen;
	uint8_t packet_type;
	uint16_t packet_handle;
	uint8_t *packet_data;
	size_t packet_dlen;
	int rc;

	ble_log_append(3, "BLE check notify\n");
	if (!desc)
		return -1;

	if (ble_check_socket_usable(desc) < 0)
		return -2;

	/* Get another message from the Bluetooth socket. */
	ble_log_append(4, "read() non-blocking\n");
	rdlen = ble_read(desc, buf, sizeof(buf));
	ble_log_append(5, "read() => %zd\n", rdlen);
	if (rdlen < 0)
		return -2;
	if (!rdlen)
		return 0;
	ble_log_append(4, "read() len %zd, type 0x%02x\n", rdlen, buf[0]);

	/* Get header fields and references to the payload data. */
	packet_type = 0x00;
	packet_handle = 0x0000;
	packet_data = NULL;
	packet_dlen = 0;
	if (rdlen >= 1)
		packet_type = buf[0];
	if (rdlen >= 3) {
		packet_handle = bt_get_le16(&buf[1]);
		packet_data = &buf[3];
		packet_dlen = rdlen - 3;
	}

	/* Dispatch according to the message type. */
	switch (packet_type) {
	case BLE_ATT_ERROR_RESP:
		ble_log_append(4, "error response\n");
		/* EMPTY */
		break;
	case BLE_ATT_WRITE_RESP:
		ble_log_append(4, "write response\n");
		/* EMPTY */
		break;
	case BLE_ATT_HANDLE_INDICATION:
		ble_log_append(4, "handle indication\n");
		ble_write_type(desc, BLE_ATT_HANDLE_CONFIRMATION);
		if (packet_handle != desc->read_handle)
			return -4;
		if (!packet_data)
			return -4;
		rc = desc->data_cb ? desc->data_cb(desc->data_cb_data, packet_data, packet_dlen) : 0;
		return rc;
	case BLE_ATT_HANDLE_NOTIFICATION:
		ble_log_append(4, "handle notification\n");
		if (packet_handle != desc->read_handle)
			return -4;
		if (!packet_data)
			return -4;
		rc = desc->data_cb ? desc->data_cb(desc->data_cb_data, packet_data, packet_dlen) : 0;
		return rc;
	default:
		ble_log_append(4, "unsupported type 0x%02x\n", packet_type);
		return -3;
	}

	return 0;
}

/* }}} indication/notification */
/* {{{ read/write */

ssize_t ble_write(struct ble_desc *desc, const void *data, size_t len)
{
	ssize_t wrlen;

	ble_log_append(3, "BLE write (raw)\n");
	if (!desc)
		return -1;
	if (desc->fd < 0)
		return -1;

	if (ble_check_socket_usable(desc) < 0)
		return -2;

	/* Send TX data to the writable characteristics for BLE UART services. */
	if (desc->write_handle)
		return ble_char_write_req(desc, desc->write_handle, data, len);

	/* Send raw TX data to the RFCOMM socket for BT Classic channels. */
	wrlen = write(desc->fd, data, len);
	return wrlen;
}

static ssize_t ble_write_type(struct ble_desc *desc, uint8_t type)
{
	ssize_t wrlen;

	ble_log_append(3, "BLE write (type)\n");
	if (!desc)
		return -1;
	if (desc->fd < 0)
		return -1;

	if (ble_check_socket_usable(desc) < 0)
		return -2;

	wrlen = write(desc->fd, &type, sizeof(type));
	if (wrlen < 0)
		return wrlen;
	if (wrlen < sizeof(type))
		return -1;
	return 0;
}

static ssize_t ble_write_type_handle(struct ble_desc *desc, uint8_t type, uint16_t handle)
{

	ble_log_append(3, "BLE write (type, handle)\n");
	return ble_write_type_handle_bytes(desc, type, handle, NULL, 0);
}

ssize_t ble_write_type_handle_bytes(struct ble_desc *desc, uint8_t type, uint16_t handle, const uint8_t *data, size_t len)
{
	uint8_t header[sizeof(uint8_t) + sizeof(uint16_t)];
	struct iovec iov[2] = {
		{ .iov_base = header, .iov_len = sizeof(header), },
		{ .iov_base = (void *)data, .iov_len = len, },
	};
	ssize_t wrlen;

	ble_log_append(3, "BLE write (type, handle, data)\n");
	if (!desc)
		return -1;
	if (desc->fd < 0)
		return -1;

	if (ble_check_socket_usable(desc) < 0)
		return -2;

	header[0] = type;
	bt_put_le16(handle, &header[1]);

	if (data && len)
		wrlen = writev(desc->fd, iov, ARRAY_SIZE(iov));
	else
		wrlen = write(desc->fd, header, sizeof(header));

	if (wrlen < 0)
		return wrlen;
	if (wrlen < sizeof(header))
		return -1;
	wrlen -= sizeof(header);
	return wrlen;
}

/* Returns negative upon error, or returns the number of _payload_ bytes written. */
static ssize_t ble_char_write_req(struct ble_desc *desc, uint16_t handle, const void *data, size_t len)
{

	ble_log_append(3, "BLE write-char req\n");
	return ble_write_type_handle_bytes(desc, BLE_ATT_WRITE_REQ, handle, data, len);
}

ssize_t ble_read(struct ble_desc *desc, void *data, size_t len)
{
	struct pollfd fds[1];
	int ret;
	ssize_t rdlen;

	ble_log_append(3, "BLE read (non-blocking)\n");
	if (!desc)
		return -1;
	if (desc->fd < 0)
		return -1;

	if (ble_check_socket_usable(desc) < 0)
		return -2;

	ble_log_append(4, "poll(POLLIN)\n");
	memset(fds, 0, sizeof(fds));
	fds[0].fd = desc->fd;
	fds[0].events = POLLIN;
	ret = poll(fds, ARRAY_SIZE(fds), 0);
	ble_log_append(4, "poll(%d, POLLIN) => 0x%x\n", desc->fd, fds[0].revents);
	if (ret < 0)
		return ret;
	if (!ret)
		return 0;
	if (!(fds[0].revents & POLLIN))
		return 0;

	ble_log_append(4, "read()\n");
	rdlen = read(desc->fd, data, len);
	ble_log_append(4, "read() => %zd\n", rdlen);
	return rdlen;
}

/* }}} indication/notification */
/*
 * vim:foldmethod=marker:
 */
