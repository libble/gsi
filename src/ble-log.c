/*
 * This file is part of the libble project.
 *
 * Copyright (C) 2018-2019 Gerhard Sittig <gerhard.sittig@gmx.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ble-config.h"
#include "ble.h"

#include <stdarg.h>
#include <stdio.h>

/* {{{ logging */

static int log_level;
static FILE *log_stream;

void ble_log_setup(int level) {

	log_level = level;
	log_stream = stdout;
}

void ble_log_append(int level, const char *fmt, ...) {
	va_list arg;

	if (level > log_level)
		return;
	if (!log_stream)
		return;
	va_start(arg, fmt);
	vfprintf(log_stream, fmt, arg);
	va_end(arg);
	fflush(log_stream);
}

/* }}} logging */
/*
 * vim:foldmethod=marker:
 */
