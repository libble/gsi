/*
 * This file is part of the libble project.
 *
 * Copyright (C) 2018-2019 Gerhard Sittig <gerhard.sittig@gmx.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Libble main test suite.  "Main application", demonstrating how to
 * use the library.
 */

/* {{{ preliminaries: includes, defines, globals */

#include "ble.h"

#include <libgen.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define DEFAULT_MAC_ADDR	NULL
#define DEFAULT_READ_HANDLE	8
#define DEFAULT_WRITE_HANDLE	0
#define DEFAULT_INIT_HANDLE	9
#define DEFAULT_INIT_VALUE	0x0003

#ifndef ARRAY_SIZE
#  define ARRAY_SIZE(x)		(sizeof(x) / sizeof(x[0]))
#endif

static char *progname;
static int verbose = 1;
static int raw_bytes = 0;

/* }}} preliminaries: includes, defines, globals */
/* {{{ BT and BLE scan support */

static void scan_cb(void *cb_data, const char *addr, const char *name)
{
	char *text;

	text = cb_data;
	ble_log_append(1, "found: type %s, addr %s, name %s\n", text, addr, name);
}

static int do_scan(struct ble_desc *desc, int duration)
{
	int rc;

	ble_log_append(1, "scan: classic\n");
	rc = ble_config_cb_scan(desc, scan_cb, "BT");
	if (rc < 0)
		return -1;
	rc = ble_scan_bt(desc, duration);
	if (rc < 0)
		return -1;

	ble_log_append(1, "scan: low energy\n");
	rc = ble_config_cb_scan(desc, scan_cb, "LE");
	if (rc < 0)
		return -1;
	rc = ble_scan_le(desc, duration);
	if (rc < 0)
		return -1;

	return 0;
}

/* }}} BT and BLE scan support */
/* {{{ BT and BLE comm support */

static int do_rfcomm(struct ble_desc *desc, int read_delay)
{
	static const char *texts[] = {
		"Hi server, I am the client.",
		"This is the first line of text.",
		"Here is another line of text.",
		"Bye server, nice talking to you.",
		NULL,
	};
	size_t idx;
	const char *text;
	char buff[256];
	int rc;

	rc = ble_connect_rfcomm(desc);
	if (rc < 0)
		return -2;
	for (idx = 0; text = texts[idx]; idx++) {
		rc = ble_write(desc, text, strlen(text));
		if (rc > 0)
			ble_log_append(0, "sent: %s\n", text);
		do {
			rc = ble_read(desc, buff, sizeof(buff));
			if (rc > 0) {
				buff[rc] = '\0';
				ble_log_append(0, "rcvd: %s\n", buff);
			}
		} while (rc > 0);
		if (read_delay) {
			usleep(read_delay * 1000);
			do {
				rc = ble_read(desc, buff, sizeof(buff));
				if (rc > 0) {
					buff[rc] = '\0';
					ble_log_append(0, "rcvd: %s\n", buff);
				}
			} while (rc > 0);
		}
	}
	ble_disconnect(desc);
	return 0;
}

/* No processing, mere dump of received payload bytes. */
int process(void *cb_data, uint8_t *data, size_t len)
{

	(void)cb_data;

	if (raw_bytes) {
		fwrite(data, sizeof(uint8_t), len, stdout);
		fflush(stdout);
		return 0;
	}

	ble_log_append(1, "BLE payload data: %zu bytes", len);
	if (len)
		ble_log_append(1, ":");
	while (len-- > 0)
		ble_log_append(1, " %02x", *data++);
	ble_log_append(1, "\n");
	fflush(stdout);

	return 0;
}

static int do_ble_indnot(struct ble_desc *desc, int read_delay)
{
	int rc;

	rc = ble_connect_ble(desc);
	if (rc) {
		ble_log_append(0, "Failed to connect, rc %d\n", rc);
		return -2;
	}
	ble_log_append(3, "Connected.\n");

	rc = ble_config_cb_data(desc, process, NULL);
	if (rc < 0)
		return -2;

	/* Initiate reception of data, and have the received data processed. */
	rc = ble_start_notify(desc);
	if (rc < 0) {
		ble_log_append(0, "Failed to setup notifications, rc %d\n", rc);
		return -1;
	}
	ble_log_append(3, "Receiving notifications.\n");
	while (rc >= 0) {
		rc = ble_check_notify(desc);
		if (rc < 0)
			ble_log_append(0, "Failed to check notifications, rc %d\n", rc);
		if (read_delay) {
			ble_log_append(5, "usleep() start\n");
			usleep(read_delay * 1000);
			ble_log_append(5, "usleep() done\n");
		}
	}
	ble_log_append(3, "Done receiving.\n");

	ble_log_append(3, "Disconnecting.\n");
	ble_disconnect(desc);
	return 0;
}

/* }}} BT and BLE comm support */
/* {{{ main program */

static void synopsis(FILE *f)
{

	fprintf(f, "synopsis: %s [-h] [-vq] [-i <addr>] [-I <nr>] [-s <len>] [-m <addr>] [-R <nr>] [-c <hdl>] [-C <val>] [-r <hdl>] [-w <hdl>] [-b] [-d <msec>]\n", progname);
	fprintf(f, "options:\n");
	fprintf(f, "\t-h\tprint builtin help (this text)\n");
	fprintf(f, "\t-v, -q\tadjust verbosity\n");
	fprintf(f, "\t-i, -I\tspecify local adapter to use\n");
	fprintf(f, "\t-s\tscan for the specified length (number times 1.28s)\n");
	fprintf(f, "\t-m\tspecify (remote) device address\n");
	fprintf(f, "\t-R\tconnect to this channel of an RFCOMM server\n");
	fprintf(f, "\t-c, -C\tspecify init handle/value (CCCD)\n");
	fprintf(f, "\t-r, -w\tspecify read/write handle (RX, TX)\n");
	fprintf(f, "\t-b\tdump raw bytes to output (text by default)\n");
	fprintf(f, "\t-d\tdelay between reads for this many msecs\n");
}

int main(int argc, char **argv)
{
	int scan_time;
	int read_delay;
	const char *local_mac;
	const char *remote_mac;
	size_t rfcomm_channel;
	uint16_t read_handle, write_handle;
	uint16_t init_handle, init_value;
	int c, num;
	struct ble_desc *desc;
	int rc;

	/* Preset defaults. */
	progname = basename(argv[0]);
	ble_log_setup(verbose);
	scan_time = 0;
	read_delay = 0;
	local_mac = NULL;
	remote_mac = DEFAULT_MAC_ADDR;
	rfcomm_channel = 0;
	read_handle = DEFAULT_READ_HANDLE;
	write_handle = DEFAULT_WRITE_HANDLE;
	init_handle = DEFAULT_INIT_HANDLE;
	init_value = DEFAULT_INIT_VALUE;

	/* Scan command line arguments. */
	while ((c = getopt(argc, argv, "bc:C:d:hi:I:m:qr:R:s:vw:")) != EOF) {
		switch (c) {
		case 'b':
			raw_bytes = 1;
			break;
		case 'c':
			init_handle = strtol(optarg, NULL, 0);
			break;
		case 'C':
			init_value = strtol(optarg, NULL, 0);
			break;
		case 'd':
			read_delay = strtol(optarg, NULL, 0);
			break;
		case 'h':
			synopsis(stdout);
			return 0;
		case 'i':
			if (local_mac)
				free((void *)local_mac);
			local_mac = strdup(optarg);
			break;
		case 'I':
			if (local_mac)
				free((void *)local_mac);
			num = strtol(optarg, NULL, 0);
			local_mac = ble_adapter_get_address(num);
			if (!local_mac) {
				ble_log_append(0, "cannot get address for HCI dev %d\n", num);
				return -1;
			}
			break;
		case 'm':
			remote_mac = optarg;
			break;
		case 'q':
			verbose--;
			break;
		case 'r':
			read_handle = strtol(optarg, NULL, 0);
			break;
		case 'R':
			rfcomm_channel = strtol(optarg, NULL, 0);
			break;
		case 's':
			scan_time = strtol(optarg, NULL, 0);
			break;
		case 'v':
			verbose++;
			break;
		case 'w':
			write_handle = strtol(optarg, NULL, 0);
			break;
		default:
			synopsis(stderr);
			return -1;
		}
	}
	argc -= optind;
	argv += optind;
	ble_log_setup(verbose);

	desc = ble_desc_new();
	if (!desc) {
		ble_log_append(0, "Failed to create BLE descriptor.\n");
		return -1;
	}
	if (local_mac) {
		ble_log_append(2, "local addr '%s'\n", local_mac);
		rc = ble_config_addr_local(desc, local_mac);
		if (rc < 0)
			return -1;
	}
	if (remote_mac) {
		ble_log_append(2, "device addr '%s'\n", remote_mac);
		rc = ble_config_addr_remote(desc, remote_mac);
		if (rc < 0)
			return -1;
	}
	ble_log_append(3, "Preparing.\n");

	if (scan_time) {
		ble_log_append(2, "scanning for devices\n");
		rc = do_scan(desc, scan_time);
	} else if (rfcomm_channel) {
		ble_log_append(2, "RFCOMM client, channel %d\n", rfcomm_channel);
		rc = ble_config_rfcomm(desc, rfcomm_channel);
		if (rc < 0)
			return -1;
		rc = do_rfcomm(desc, read_delay);
	} else {
		ble_log_append(2, "indications/notifications\n");
		ble_log_append(2, "read/write hdl %u/%u, init hdl/val %u/%u\n", read_handle, write_handle, init_handle, init_value);
		rc = ble_config_notify(desc, read_handle, write_handle, init_handle, init_value);
		if (rc < 0)
			return -1;
		rc = do_ble_indnot(desc, read_delay);
	}

	ble_desc_free(desc);
	return 0;
}

/* }}} main program */
/*
 * vim:foldmethod=marker:
 */
