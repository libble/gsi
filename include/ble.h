/*
 * This file is part of the libble project.
 *
 * Copyright (C) 2018-2019 Gerhard Sittig <gerhard.sittig@gmx.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BLE_H
#define BLE_H

#include <stdint.h>
#include <stdlib.h>

int ble_get_version_info(const char **lib_name, const char **lib_vers);
int ble_get_system_info(const char **sys_name, const char **sys_vers);

const char *ble_adapter_get_address(size_t idx);

struct ble_desc;
typedef void (*ble_scan_cb)(void *cb_data, const char *addr, const char *name);
typedef int (*ble_data_cb)(void *cb_data, uint8_t *data, size_t dlen);

struct ble_desc *ble_desc_new(void);
void ble_desc_free(struct ble_desc *desc);

int ble_config_cb_scan(struct ble_desc *desc, ble_scan_cb cb, void *cb_data);
int ble_config_cb_data(struct ble_desc *desc, ble_data_cb cb, void *cb_data);
int ble_config_addr_local(struct ble_desc *desc, const char *addr);
int ble_config_addr_remote(struct ble_desc *desc, const char *addr);
int ble_config_rfcomm(struct ble_desc *desc, size_t channel);
int ble_config_notify(struct ble_desc *desc, uint16_t read_handle, uint16_t write_handle, uint16_t init_handle, uint16_t init_value);

int ble_scan_le(struct ble_desc *desc, int duration);
int ble_scan_bt(struct ble_desc *desc, int duration);

int ble_connect_ble(struct ble_desc *desc);
int ble_connect_rfcomm(struct ble_desc *desc);
void ble_disconnect(struct ble_desc *desc);

ssize_t ble_read(struct ble_desc *desc, void *data, size_t len);
ssize_t ble_write(struct ble_desc *desc, const void *data, size_t len);

int ble_start_notify(struct ble_desc *desc);
int ble_check_notify(struct ble_desc *desc);

void ble_log_setup(int level);
void ble_log_append(int level, const char *fmt, ...);

#endif /* BLE_H */
/*
 * vim:foldmethod=marker:
 */
